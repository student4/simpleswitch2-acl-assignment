# Copyright (c) 2017 NoviFlow Inc
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"Test ACL functionality as per the Inside-NoviFlow assignment"

import unittest
import sys
import os
import time
import tempfile
import pexpect

DEBUG = os.getenv('TEST_ACL_DEBUG')
TSHARK_TIMEOUT = 0.5
MAC_H1 = '00:00:00:00:00:01'


def debug(p):
    if not DEBUG:
        return
    p.logfile_read = sys.stdout


def cleanup():
    p = pexpect.spawn('mn -c')
    debug(p)
    p.expect('Cleanup complete')


def setUpModule():
    cleanup()
    scapy = pexpect.which('scapy')
    if scapy is None:
        raise RuntimeError('Scapy does not appear to be installed')


class ExpectTestCase(unittest.TestCase):
    "Base class used for ACL tests"
    ryuParams = ['--verbose', 'ss2.core']
    mnParams = [
        '--topo', 'single,2',
        '--mac',
        '--switch', 'ovsk',
        '--controller', 'remote']
    prompt = "mininet>"
    tsharkCmd = "tshark -i s1-eth1 -l -n ether dst %s" % MAC_H1

    @classmethod
    def startRyu(cls):
        cls.ryuP = pexpect.spawn('ryu-manager', cls.ryuParams, timeout=600)
        debug(cls.ryuP)
        cls.ryuP.expect('loading app', timeout=10)
        return cls.ryuP

    @classmethod
    def stopRyu(cls):
        if cls.ryuP is None:
            return

        cls.ryuP.sendintr()
        cls.ryuP = None

    @classmethod
    def startMininet(cls):
        cls.mnP = pexpect.spawn('mn', cls.mnParams)
        debug(cls.mnP)
        cls.mnP.expect(cls.prompt)
        return cls.mnP

    @classmethod
    def stopMininet(cls):
        if cls.mnP is None:
            return

        cls.mnP.sendline('exit')
        cls.mnP.wait()
        cls.mnP = None

    @classmethod
    def setUpClass(cls):
        cls.startRyu()
        cls.startMininet()
        # Wait for OVS to connect to Ryu
        cls.ryuP.expect('connected socket')

    @classmethod
    def tearDownClass(cls):
        "Clean up the network"
        cls.stopRyu()
        cls.stopMininet()
        if sys.exc_info != (None, None, None):
            cleanup()

    def expectPacket(self, command, expected):
        "Starts tshark, runs scapy, checks if packet went through"
        tsharkP = pexpect.spawn(self.tsharkCmd)
        debug(tsharkP)

        # Wait for tshark to be ready for capture
        tsharkP.expect('Capturing on ')

        # Start scapy in h2 and set its primary interface
        self.mnP.send('h2 scapy')
        self.mnP.send('\r\n')
        self.mnP.expect('>>>')
        self.mnP.send('conf.iface = "h2-eth0"')
        self.mnP.send('\r\n')
        self.mnP.expect('>>>')

        # Send the scapy command specified and check for failure
        self.mnP.send(command)
        self.mnP.send('\r\n')
        e = self.mnP.expect(['Traceback', '>>>'])
        if e == 0:
            self.fail('Unexpected Tracback from scapy')
            return

        # Exit scapy and wait for mininet
        self.mnP.send('exit()')
        self.mnP.send('\r\n')
        self.mnP.expect(self.prompt)

        # Check output of tshark for expected output and return result
        out = False
        try:
            tsharkP.expect(expected, timeout=TSHARK_TIMEOUT)
            out = True
        except pexpect.TIMEOUT:
            pass
        finally:
            tsharkP.sendintr()

        return out


class AclDropMacTestCase(ExpectTestCase):
    """MAC block test.

    NOTE: These check for blocking the SOURCE mac address, not destination.

    Test ACL implementation of blocking MAC addresses:
     - Out of 00:01:00:00:00:00/16, only 00:01:00:03:00:00 should be dropped.
     - Out of 00:02:00:00:00:00/16, the 00:02:02:00:00:00/24 (OUI of 00:02:02)
       block of MAC addresses should be blocked.
    """
    cases_single = [
        ['00:01:00:01:00:00', True],
        ['00:01:00:02:00:00', True],
        # Only 00:00:00:03:00:00 should be dropped
        ['00:01:00:03:00:00', False],
        ['00:01:00:04:00:00', True]
    ]

    cases_mask = [
        ['00:02:01:00:10:00', True],
        ['00:02:02:00:00:01', False],
        ['00:02:02:00:10:00', False],
        ['00:02:03:10:00:00', True]
    ]

    def test_macDropSingle(self):
        for case in self.cases_single:
            self.expectEthDstPacket(*case)

    def test_macDropMask(self):
        for case in self.cases_mask:
            self.expectEthDstPacket(*case)

    def expectEthDstPacket(self, mac, shouldPass):
        e = self.expectPacket(
            command="sendp(Ether(src='%s', dst='%s'))" % (mac, MAC_H1),
            expected=mac)
        if e is not shouldPass:
            if shouldPass:
                self.fail("Expected %s to be forwarded, but packet was blocked"
                          % mac)
            else:
                self.fail("Expected %s to be blocked, but packet passed"
                          % mac)


class AclDropIPv4TestCase(ExpectTestCase):
    """IP block test.

    NOTE: These check for blocking the SOURCE IP address (like a blacklist)

    Test ACL implementation by blocking IP addresses:
     - Out of 192.168.10.0/24, only 192.168.10.5 should be dropped.
     - Out of 10.10.0.0/16, 10.10.5.0/24 should be dropped.
    """

    cases_single = [
        ["192.168.10.1", True],
        ["192.168.10.3", True],
        ["192.168.10.5", False],
        ["192.168.10.6", True],
    ]

    cases_mask = [
        ["10.10.1.1", True],
        ["10.10.5.1", False],
        ["10.10.5.3", False],
        ["10.10.6.1", True],
    ]

    def test_ipv4DropSingle(self):
        for case in self.cases_single:
            self.expectIpv4DstPacket(*case)

    def test_ipv4DropMask(self):
        for case in self.cases_mask:
            self.expectIpv4DstPacket(*case)

    def expectIpv4DstPacket(self, ipv4, shouldPass):
        e = self.expectPacket(
            command="sendp(Ether(dst='%s')"
                    "/IP(src='%s'))" % (MAC_H1, ipv4),
            expected=ipv4)
        if e is not shouldPass:
            if shouldPass:
                self.fail("Expected %s to be forwarded, but packet was blocked"
                          % ipv4)
            else:
                self.fail("Expected %s to be blocked, but packet passed"
                          % ipv4)


class AclDropPortTestCase(ExpectTestCase):
    """Port block test.

    Test ACL implementation by blocking destination ports:
     - TCP Port 139 should be blocked
     - UDP Port 137 should be blocked
     - All others should pass
    """

    cases = [
        ["TCP", 22, True],
        ["UDP", 5555, True],
        ["TCP", 139, False],
        ["UDP", 137, False],
    ]

    def test_portDropSingle(self):
        for case in self.cases:
            self.expectPortPacket(*case)

    def expectPortPacket(self, proto, port, shouldPass):
        e = self.expectPacket(
            command="sendp(Ether(dst='%s')/IP(dst='192.168.20.1')/"
                    "%s(dport=%s))" % (MAC_H1, proto, port),
            expected="%s [^\\r\\n]+ %d" % (proto, port))
        if e is not shouldPass:
            if shouldPass:
                self.fail("Expected %s:%d to be forwarded, but packet was "
                          "blocked" % (proto, port))
            else:
                self.fail("Expected %s:%d to be blocked, but packet passed"
                          % (proto, port))


class AclDropIcmpType(ExpectTestCase):
    """ICMP Code block test.

    Test ACL implementation by blocking ICMP Echo Request, but no other type.
    """
    # TODO: writeme
    cases = [
        [0, True],  # ICMP Echo Reply
        [9, True],  # Router Advertisement
        [11, True],  # TTL exceeded
        [8, False],  # ICMP Echo Request (Block)
    ]

    def test_icmpTypeDrop(self):
        for case in self.cases:
            self.expectIcmpTypePacket(*case)

    def expectIcmpTypePacket(self, type_, shouldPass):
        e = self.expectPacket(
            command="sendp(Ether(dst='%s')/IP()/ICMP(type=%d))"
                    % (MAC_H1, type_),
            expected="ICMP")
        if e is not shouldPass:
            if shouldPass:
                self.fail("Expected type %d to be forwarded, but packet was "
                          "blocked" % type_)
            else:
                self.fail("Expected type %d to be blocked, but packet passed"
                          % type_)
