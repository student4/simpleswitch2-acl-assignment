"Test basic ACLs"

import unittest
import sys
import time

from threading import Thread

from mininet.net import Mininet
from mininet.node import Ryu, OVSSwitch
from mininet.clean import cleanup
from mininet.topo import Topo, SingleSwitchTopo
from mininet.cli import CLI

from scapy.all import sniff

# pylint: disable=C0111,R0201

MAC_H1 = '00:00:00:00:00:01'
MAC_H2 = '00:00:00:00:00:02'

NAME_H1 = 'h1'
NAME_H2 = 'h2'
NAME_S1 = 's1'

INTF_H1 = 'h1-eth0'
INTF_H2 = 'h2-eth0'
INTF_S1 = {NAME_H1: 's1-eth1', NAME_H2: 's1-eth2'}

def setUpModule():
    cleanup()

class MinimalTopo(Topo):
    "Minimal topology with a single switch and two hosts"

    def build(self):
        # Create two hosts.
        h1 = self.addHost(NAME_H1)
        h2 = self.addHost(NAME_H2)

        # Create a switch
        s1 = self.addSwitch(NAME_S1)

        # Add links between the switch and each host
        self.addLink(s1, h1)
        self.addLink(s1, h2)


class BaseTestCase(unittest.TestCase):
    ryuParams = ['ss2.core']

    def controller(self, name, **params):
        return Ryu(name, *self.ryuParams, **params)

    def setUp(self):
        self.net = Mininet(topo=MinimalTopo(),
                           controller=self.controller,
                           switch=OVSSwitch,
                           waitConnected=True,
                           autoSetMacs=True)

        self.net.start()

    def tearDown(self):
        "Clean up the network"
        if sys.exc_info != (None, None, None):
            cleanup()
        else:
            self.net.stop()

    def packet_received(self):
        return 0 < len(sniff(iface=INTF_S1[NAME_H1], count=1, timeout=2, filter="ether dst %s"%(MAC_H1)))

    def send_packet_thread(self, packet, wait=1):
        time.sleep(wait)
        # Nastiness - it would be better if I didn't have to call out to python like this...
        sendPktCmd = 'from scapy.all import *; conf.iface = "%s"; sendp(%s)'%(INTF_H2, packet)
        self.net[NAME_H2].cmd('python -c \'' + sendPktCmd + '\'')

    def send_packet(self, packet, wait=1):
        Thread(target=self.send_packet_thread, args = (packet, )).start()

class ACLDropIPv4(BaseTestCase):
    """IP block test.

    NOTE: These check for blocking the SOURCE IP address (like a blacklist)

    Test ACL implementation by blocking IP addresses:
     - Out of 192.168.10.0/24, only 192.168.10.5 should be dropped.
     - Out of 10.10.0.0/16, 10.10.5.0/24 should be dropped.
    """

    cases_single = [
        ["192.168.10.1", True],
        ["192.168.10.3", True],
        ["192.168.10.5", False],
        ["192.168.10.6", True],
    ]

    cases_mask = [
        ["10.10.1.1", True],
        ["10.10.5.1", False],
        ["10.10.5.3", False],
        ["10.10.6.1", True],
    ]

    def run_test(self, ipv4, expected):
        self.send_packet('Ether(dst="%s")/IP(src="%s")'%(MAC_H1, ipv4))
        if self.packet_received():
            if not expected:
                self.fail("Received %s, but it should have been blocked"%(ipv4))
        else:
            if expected:
                self.fail("Blocked %s, but it should have been received"%(ipv4))

    def test_single(self):
        for case in self.cases_single:
            self.run_test(*case)

    def test_mask(self):
        for case in self.cases_mask:
            self.run_test(*case)

class ACLDropICMPType(BaseTestCase):
    """ICMP Code block test.

    Test ACL implementation by blocking ICMP Echo Request, but no other type.
    """

    cases = [
        [0, True],  # ICMP Echo Reply
        [9, True],  # Router Advertisement
        [11, True],  # TTL exceeded
        [8, False],  # ICMP Echo Request (Block)
    ]

    def run_test(self, icmp_type, expected):
        self.send_packet('Ether(dst="%s")/IP()/ICMP(type=%d)'%(MAC_H1, icmp_type))
        if self.packet_received():
            if not expected:
                self.fail("Received type %s, but it should have been blocked"%(icmp_type))
        else:
            if expected:
                self.fail("Blocked type %s, but it should have been received"%(icmp_type))

    def test_icmp(self):
        for case in self.cases:
            self.run_test(*case)


class ACLDropMAC(BaseTestCase):
    """MAC block test.

    NOTE: These check for blocking the SOURCE mac address, not destination.

    Test ACL implementation of blocking MAC addresses:
     - Out of 00:01:00:00:00:00/16, only 00:01:00:03:00:00 should be dropped.
     - Out of 00:02:00:00:00:00/16, the 00:02:02:00:00:00/24 (OUI of 00:02:02)
       block of MAC addresses should be blocked.
    """
    cases_single = [
        ['00:01:00:01:00:00', True],
        ['00:01:00:02:00:00', True],
        # Only 00:00:00:03:00:00 should be dropped
        ['00:01:00:03:00:00', False],
        ['00:01:00:04:00:00', True]
    ]

    cases_mask = [
        ['00:02:01:00:10:00', True],
        ['00:02:02:00:00:01', False],
        ['00:02:02:00:10:00', False],
        ['00:02:03:10:00:00', True]
    ]

    def run_test(self, mac, expected):
        self.send_packet('Ether(dst="%s", src="%s")'%(MAC_H1, mac))
        if self.packet_received():
            if not expected:
                self.fail("Received %s, but it should have been blocked"%(mac))
        else:
            if expected:
                self.fail("Blocked %s, but it should have been received"%(mac))


    def test_macDropSingle(self):
        for case in self.cases_single:
            self.run_test(*case)

    def test_macDropMask(self):
        for case in self.cases_mask:
            self.run_test(*case)

class ACLDropPort(BaseTestCase):
    """Port block test.

    Test ACL implementation by blocking destination ports:
     - TCP Port 139 should be blocked
     - UDP Port 137 should be blocked
     - All others should pass
    """

    cases = [
        ["TCP", 22, True],
        ["UDP", 5555, True],
        ["TCP", 139, False],
        ["UDP", 137, False],
    ]

    def run_test(self, proto, port, expected):
        self.send_packet('Ether(dst="%s")/IP(dst="192.168.20.1")/%s(dport=%d)'%(MAC_H1, proto, port))
        if self.packet_received():
            if not expected:
                self.fail("Received %s port %d, but it should have been blocked"%(proto, port))
        else:
            if expected:
                self.fail("Blocked %s port %d, but it should have been received"%(proto, port))

    def test_portDropSingle(self):
        for case in self.cases:
            self.run_test(*case)
